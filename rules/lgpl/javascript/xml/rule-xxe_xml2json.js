// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/ajinabraham/njsscan/blob/master/tests/assets/node_source/true_positives/semantic_grep/xml/xxe_xml2json.js
// hash: e7a0a61
function test1() {
    const express = require('express')
    const xml2json = require('xml2json')
    const app = express()
    const port = 3000

    app.get('/', (req, res) => {
        // ruleid: rules_lgpl_javascript_xml_rule-xxe-xml2json
        const xml = req.query.xml
        const content = xml2json.toJson(xml, { coerce: true, object: true });
    })

    app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
}

function test2() {
    const express = require('express')
    const xml2json = require('xml2json')
    const app = express()
    const port = 3000

    app.get('/', (req, res) => {
        // ruleid: rules_lgpl_javascript_xml_rule-xxe-xml2json
        const content = xml2json.toJson(req.body, { coerce: true, object: true });
    })

    app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
}
