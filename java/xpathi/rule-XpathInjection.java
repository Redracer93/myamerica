// License: LGPL-3.0 License (c) find-sec-bugs
package xpathi;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import javax.xml.xpath.*;
import java.util.HashMap;

// ref: java_xpathi_rule-XpathInjection
public class XpathInjection {

    public static void main(String[] args) throws Exception {

        Document doc = null;
        String input = args.length != 0 ? args[1] : "guess' or '1'='1";
        String query = "//groups/group[@id='" + input + "']/writeAccess/text()";

        System.out.println(">> XPath.compile()");
        {
            XPath xpath = XPathFactory.newInstance().newXPath();
            // ruleid: java_xpathi_rule-XpathInjection
            XPathExpression expr = xpath.compile(query);
        }

        System.out.println(">> XPath.evaluate()");
        {
            XPath xpath = XPathFactory.newInstance().newXPath();
            // ruleid: java_xpathi_rule-XpathInjection
            String result = xpath.evaluate(query, doc);
            System.out.println("result=" + result);
        }

        //Safe (The next sample should not be mark)
        System.out.println(">> Safe");
        {
            XPath xpath = XPathFactory.newInstance().newXPath();
            // ok: java_xpathi_rule-XpathInjection
            XPathExpression expr = xpath.compile("//groups/group[@id='admin']/writeAccess/text()");
        }
    }

    public static void test(String[] args) throws Exception {

        Document doc = null;
        String input = args.length != 0 ? args[1] : "guess' or '1'='1";
        String query = "//groups/group[@id='" + input + "']/writeAccess/text()";
        String query2 = "//author[contains(., $author)]";

        System.out.println(">> XPath.compile()");
        {
            XPath xpath = XPathFactory.newInstance().newXPath();
            SimpleXPathVariableResolver resolver = new SimpleXPathVariableResolver();
            resolver.setVariable(new QName("author"), input);
            xpath.setXPathVariableResolver(resolver);

            // ok: java_xpathi_rule-XpathInjection
            String result = xpath.compile("//groups/group[@id='admin']/writeAccess/text()").evaluate(doc);
            // ok: java_xpathi_rule-XpathInjection
            result = xpath.compile(query2).evaluate(doc);
            // ok: java_xpathi_rule-XpathInjection
            result = xpath.compile("//author[contains(., $author)]").evaluate(doc);

            // ruleid: java_xpathi_rule-XpathInjection
            result = xpath.compile(query).evaluate(doc);
            // ruleid: java_xpathi_rule-XpathInjection
            result = xpath.compile("//groups/group[@id='" + input + "']/writeAccess/text()").evaluate(doc);
            // ruleid: java_xpathi_rule-XpathInjection
            result = xpath.compile(query).evaluate(doc);
            // ruleid: java_xpathi_rule-XpathInjection
            result = xpath.compile("//author[contains(., $author)]" + input).evaluate(doc);
        }

    }

    public static NodeList evaluateXPath(Document doc, XPathExpression xpath) throws XPathExpressionException {
        return (NodeList) xpath.evaluate(doc, XPathConstants.NODESET);
    }

}

class SimpleXPathVariableResolver implements XPathVariableResolver {
    // Use a map or lookup table to store variables for resolution
    private HashMap<QName, String> variables = new HashMap<>();
    // Allow caller to set variables
    public void setVariable(QName name, String value) {
        variables.put(name, value);
    }
    // Implement the resolveVariable to return the value
    @Override
    public Object resolveVariable(QName name) {
        return variables.getOrDefault(name, "");
    }
}
