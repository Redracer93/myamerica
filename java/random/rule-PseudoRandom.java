// License: LGPL-3.0 License (c) find-sec-bugs
package random;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.util.Random;

public class PseudoRandom {
    // ruleid: java_random_rule-PseudoRandom
    static String randomVal =  Long.toHexString(new Random().nextLong());

    public static String generateSecretToken() {
        // ruleid: java_random_rule-PseudoRandom
        Random r = new Random();
        // ruleid: java_random_rule-PseudoRandom
        return Long.toHexString(r.nextLong());
    }

    public static String count() {
        // ruleid: java_random_rule-PseudoRandom
        return RandomStringUtils.random(10);
    }

    public static long getRandomVal() {
        // ruleid: java_random_rule-PseudoRandom
        return RandomUtils.nextLong();
    }
}
